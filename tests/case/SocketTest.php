<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Network;

use Nora;

/**
 * Socketのテスト
 *
 */
class SocketTest extends \PHPUnit_Framework_TestCase
{
    public function testSocket ( )
    {
        $client = Nora::Module('network')->client('smtp-gw:25');

        var_dump(
            $client->read()
        );
    }
}
