<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Network;

use Nora\Core\Module\Module;
use Nora\Module\Network\Socket\SocketClient;

/**
 * Networkモジュール
 */
class Facade extends Module
{
    protected function initModuleImpl( )
    {
    }

    /**
     * ソケットクライアントを取り出す
     */
    public function client($spec)
    {
        return $sock = SocketClient::connect($spec, $this->newScope());
    }
}
