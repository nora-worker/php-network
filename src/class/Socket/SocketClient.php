<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Network\Socket;

use Nora\Core\Event\EventClientTrait;

use Nora\Core\Component\Component;
use Nora\Core\Component\Componentable;

/**
 * SocketClient
 */
class SocketClient extends Socket
{
    use Componentable;

    const BS = 1024;

    private $con;


    static public function connect ($spec, $scope)
    {
        $con = stream_socket_client($spec, $eno, $emsg);

        if (!$con)
        {
            throw new RuntimeException($eno.' '.$emsg);
        }

        $sock = new SocketClient( );
        $sock->setScope($scope);
        $sock->con = $con;

        return $sock;
    }

    public function initComponentImpl( )
    {
    }

    public function crypt($mode = null)
    {
        if ($mode === null)
        {
            stream_socket_enable_crypto($this->con, true);
        }
        else
        {
            stream_socket_enable_crypto($this->con, true, $mode);
        }

        $this->logDebug('crypte mode='.$mode);
    }



    public function write ($buf = '')
    {
        $buf.="\n";
        $this->logDebug('Write: '.rtrim($buf));
        $this->writeBuffer($this->con, $buf);
    }

    public function writef ($buf)
    {
        $this->write( vsprintf(
            $buf,
            array_slice(func_get_args(), 1)));
    }

    public function read ( )
    {
        $buf = $this->readBuffer($this->con);
        $this->logDebug('Read: '.$buf);
        return $buf;
    }
}
